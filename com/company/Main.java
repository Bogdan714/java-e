package com.company;

import com.company.additional.CurrencyMarket;
import com.company.additional.Generator;
import com.company.additional.Stack;
import com.company.models.Bank;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static final int N = 10;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("\nTask 1\n");
        Stack stack = new Stack(Generator.generateBooks(10));

        System.out.println("Самая старая книга:");
        stack.getOldestBook().print();

        System.out.println("Введите имя автора для поиска (например 'Author 1'): ");
        String toFind = scanner.nextLine();
        System.out.println("Введите минимальное колличество авторов: ");
        int minOfAutors = Integer.parseInt(scanner.nextLine());
        System.out.println("Результаты поиска\n____________________");
        Stack.printAll(stack.searchByAutor(toFind, minOfAutors));

        System.out.println("До какого года?");
        int year = Integer.parseInt(scanner.nextLine());//scanner.nextInt();
        stack.printTillYear(year);

        System.out.println("\nTask 2\n");
        //declaration of capitalism
        CurrencyMarket market = new CurrencyMarket();

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter name of the bank
        System.out.println("Enter the bank (Bank #[1-30]):");
        String bankName = scan.nextLine();

        //enter currency
        System.out.println("Enter the currency:");
        String currency = scan.nextLine();

        //convert uan to user currency
        market.convertAndPrint(bankName, amount, currency);
    }
}