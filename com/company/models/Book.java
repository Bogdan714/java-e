package com.company.models;

import java.util.Arrays;

public class Book {
    public String[] authors;
    public String name;
    public int year;
    public String publishing;

    public Book(String[] author, String name, int year, String publishing) {
        this.authors = author;
        this.name = name;
        this.year = year;
        this.publishing = publishing;
    }

    public void print() {
        System.out.println(name);
        System.out.println(Arrays.toString(authors));
        System.out.println(year);
        System.out.println("----------");
    }

    public boolean isOlderThen(Book book) {
        return year > book.year;
    }

    public Book compareAndReturnOldest(Book book) {
        return year > book.year ? this : book;
    }
}
