package com.company.additional;

import com.company.models.Book;

public class Stack {
    private Book[] books;

    public Stack(Book[] books) {
        this.books = books;
    }

    public Book getOldestBook() {
        int tempIndex = 0;
        for (int i = 1; i < books.length; i++) {
            if (books[i].isOlderThen(books[tempIndex])) {
                tempIndex = i;
            }
        }
        return books[tempIndex];
    }

    public void printAll() {
        for (Book book : books) {
            book.print();
        }
    }

    public static void printAll(Book[] list) {
        if(list != null) {
            for (Book book : list) {
                book.print();
            }
        }else{
            System.out.println("Список пуст");
        }
    }

    public Book[] searchByAutor(String author, int minOfAuthors) {
        boolean found = false;
        Book[] booksInHands;
        int[] shelvesId = new int[1000];
        int index = 0;
        for (int i = 0; i < books.length; i++){
            for (String writer : books[i].authors) {
                if (writer.equalsIgnoreCase(author) && books[i].authors.length >= minOfAuthors) {
                    shelvesId[index++] = i;
                    found = true;
                }
            }
        }
        if (!found) {
            System.out.println("Книг с таким автором не найдено");
            return null;
        }
        else{
            booksInHands = new Book[index];
            for (int i = 0; i < booksInHands.length; i++) {
                booksInHands[i] = books[shelvesId[i]];
            }
        }
        return booksInHands;
    }

    public void printTillYear(int year) {
        boolean found = false;
        System.out.println("Результаты поиска\n____________________");
        for (Book book : books) {
            if (book.year < year) {
                found = true;
                book.print();
            }
        }
        if (!found) {
            System.out.println("Книг такого года не найдено");
        }

    }
}
