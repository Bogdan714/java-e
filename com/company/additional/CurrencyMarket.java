package com.company.additional;

import com.company.models.Bank;
import com.company.models.Currency;

import java.util.Locale;

public class CurrencyMarket {
    Bank[] banks;

    public CurrencyMarket() {
        this.banks = Generator.generateBanks(30);
    }

    public void convertAndPrint(String bankName, int amount, String currencyName) {
        boolean bankFound = false;
        boolean currencyFound = false;
        System.out.println("Result\n____________________");
        for (Bank bank : banks) {
            if (bank.name.equalsIgnoreCase(bankName)) {
                bankFound = true;
                System.out.println("Bank \"" + bank.name + "\"");
                for (Currency currency : bank.currencies) {
                    if (currency.name.equalsIgnoreCase(currencyName)) {
                        currencyFound = true;
                        System.out.println(String.format(Locale.US, "You money in %s(buy | sell): %.2f | %.2f", currency.name, amount / currency.buy, amount / currency.sell));
                    }
                }
                System.out.println("--------------------");
            }
        }
        if (!bankFound) {
            System.err.println("Can't find bank \'" + bankName + "\'");
        }
        if (!currencyFound) {
            System.err.println("Can't find currency \'" + bankName + "\'");
        }
    }
}
