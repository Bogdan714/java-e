package com.company.additional;

import com.company.models.Bank;
import com.company.models.Book;
import com.company.models.Currency;

public class Generator {
    public static Book[] generateBooks(int n) {
        Book[] books = new Book[n];
        for (int i = 0; i < books.length; i++) {
            String[] authors = new String[(int) (Math.random() * 3) + 1];
            for (int j = 0; j < authors.length; j++) {
                authors[j] = "Author " + (j + 1);
            }
            books[i] = new Book(authors, "Book #" + i + 1, 1900 + (i * 4), "publishing #" + 2 * i);
        }
        return books;
    }

    public static Bank[] generateBanks(int n) {
        Bank[] banks = new Bank[n];
        String[] currencyNames = {"EUR", "USD", "GBP"};
        float[] changeRares = {30.0f, 25.0f, 35.0f};
        for (int i = 0; i < banks.length; i++) {
            banks[i] = new Bank();
            banks[i].name = "Bank " + (i + 1);
            int exchangeRates = currencyNames.length;
            banks[i].currencies = new Currency[exchangeRates];
            for (int j = 0; j < exchangeRates; j++) {
                banks[i].currencies[j] = new Currency();
                banks[i].currencies[j].name = currencyNames[j];
                banks[i].currencies[j].buy = changeRares[j] + ((float) (Math.random() - 1) * 2);
                banks[i].currencies[j].sell = banks[i].currencies[j].buy - 0.5f;
            }
        }
        return banks;
    }
}
